import java.util.Scanner;

public class Main {
    public static String calc(String input){
        Converter converter = new Converter();
        String[] actions = {"+", "-", "/", "*"};
        String[] regexActions = {"\\+", "-", "/", "\\*"};
        int actionIndex=-1;
        for (int i = 0; i < actions.length; i++) {
            if (input.contains(actions[i])) {
                actionIndex = i;
            }
        }
        if(actionIndex==-1){
            try {
                throw new Exception();
            } catch (Exception e) {
                return "Строка не является математической операцией";
            }
        }
        String[] data = input.split(regexActions[actionIndex]);
        if(converter.isRoman(data[0]) == converter.isRoman(data[1])){
            int a,b;
            boolean isRoman = converter.isRoman(data[0]);
            if(isRoman){
                a = converter.romanToInt(data[0]);
                b = converter.romanToInt(data[1]);
            }else {
                a = Integer.parseInt(data[0]);
                b = Integer.parseInt(data[1]);
            }
            int result;
            switch (actions[actionIndex]) {
                case "+":
                    result = a + b;
                    break;
                case "-":
                    result = a - b;
                    break;
                case "*":
                    result = a * b;
                    break;
                default:
                    result = a / b;
                    break;
            }
            if(isRoman){
                if(result<0){
                    try {
                        throw new Exception();
                    } catch (Exception e) {
                        return "В римской системе нет отрицательных чисел";
                    }

                }else {
                    return converter.intToRoman(result);
                }
            }else{
                return String.valueOf(result);
            }
        }else{
            try {
                throw new Exception();
            } catch (Exception e) {
                return "Используются одновременно разные сисчтемы счисления";
            }
        }
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(calc(scanner.nextLine()));
    }
}
